/*
 * $Id: df1b2h.cpp 542 2012-07-10 21:04:06Z johnoel $
 *
 * Author: David Fournier
 * Copyright (c) 2008-2012 Regents of the University of California 
 */
/**
 * \file
 * Description not yet available.
 */
#if defined(__GNUDOS__)
#  pragma implementation "df1b2fun.h"
#endif
#define HOME_VERSION
#include "df1b2fun.h"

  int make_sure_df1b2fun_gets_linked=0;

#undef HOME_VERSION

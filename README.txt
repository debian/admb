ADMB-11.1 (Released May 10, 2013)

ADMB Project: http://www.admb-project.org/

Overview
========
The ADMB (Automatic Differentiation Model Builder) software suite is an
environment for nonlinear statistical modeling enabling rapid model
development, numerical stability, fast and efficient computation, and high
accuracy parameter estimates. AD Model Builder is a high level language built
around the AUTODIF Library, a C++ language extension which transparently
implements reverse mode automatic differentiation. A closely related software
package, ADMB-RE, implements random effects in nonlinear models.

Donations
=========
ADMB is an open source project that is freely available for download. If you
would like to contribute or donate funds, please contact users@admb-project.org.
Cool ADMB T-shirts are also available for purchase.

How to Cite ADMB
================
Fournier, D.A., H.J. Skaug, J. Ancheta, J. Ianelli, A. Magnusson, M.N. Maunder,
A. Nielsen, and J. Sibert. 2012. AD Model Builder: using automatic
differentiation for statistical inference of highly parameterized complex
nonlinear models. Optim. Methods Softw. 27:233-249.

It can be downloaded for free from the publisher:
'http://tandfonline.com/doi/abs/10.1080/10556788.2011.597854'.

Documentation
=============
Release notes are in CHANGES.txt.

Installation procedures are in INSTALL.txt.

User manuals for AD Model Builder, the AUTODIF library and the ADMB-RE nonlinear
random effects module can be downloaded from
'http://www.admb-project.org/downloads/'.

Additional documentation is also available on the ADMB website
'http://www.admb-project.org/'.

Help and Support
================
For help and support, email 'users@admb-project.org'.

User list archives are available from
'http://lists.admb-project.org/pipermail/users/'.

Developers list archives are available from
'http://lists.admb-project.org/pipermail/developers/'.

Contributors
============
This software was originally developed by David Fournier of Otter Research Ltd.

Several other folks have contributed to the ADMB Project:

David Fournier <davef@otter-rsch.com>
John Sibert <sibert@hawaii.edu>
Hans Skaug <Hans.Skaug@mi.uib.no>
Mark Maunder <mmaunder@iattc.org>
Anders Nielsen <anders@nielsensweb.org>
Arni Magnusson <arnima@hafro.is>
Ian Taylor <ian.taylor@noaa.gov>
Chris Grandin <Chris.Grandin@dfo-mpo.gc.ca>
Derek Seiple <dseiple@hawaii.edu>
Johnoel Ancheta <johnoel@hawaii.edu>

Thanks to the following people for providing fixes and suggestions.
 * Dave Fournier
 * Jan Jaap Poos
 * Gareth Williams
 * Weihai Liu
 * Barak A. Pearlmutter

--------------------------------------------------------------------------------
$ID$
